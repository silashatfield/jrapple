package com.jrapple.service;

import java.util.ArrayList;

class route{
    String name;
    String path;
    route(String name, String path){
        this.name = name;
        this.path = path;
    }
}

class header{
    String key;
    String value;
    header(String key, String value){
        this.key = key;
        this.value = value;
    }
}

public class provider {

    String name;
    ArrayList<header> headers = new ArrayList<header>();
    ArrayList<route> routes = new ArrayList<route>();

    public header newheader(String key, String value){
        return new header(key, value);
    }

    public route newroute(String name, String path){
        return new route(name, path);
    }

}
