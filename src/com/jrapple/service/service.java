package com.jrapple.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.boris.winrun4j.AbstractService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;


public class service extends AbstractService {
    private static String ConfigFileName = "config\\config.json";
    private static String AccessLogFileName = "logs\\access_log.txt";
    private static String ErrorLogFileName = "logs\\error_log.txt";
    private static String ProviderDefaultFileName = "providers\\bitbucket-push.json";
    public static ServerSocket Socket = null;

    public service(){

    }

    public static void main(String[] args){

        while(true) {
            final service service = new service();
            try {
                service.createDefaultProvider();
                service.connect();
                Runtime.getRuntime().addShutdownHook(
                        new Thread() {
                            public void run() {
                                try {
                                    service.close();
                                } catch (IOException e) {
                                }
                            }
                        }
                );
                while (true) {
                    Socket clientSocket = service.Socket.accept();
                    client client = new client(clientSocket);
                    client.start();
                }
            } catch (Exception e) {
                service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
                e.printStackTrace();
            } finally {
                if (service.Socket != null && !service.Socket.isClosed()) {
                    try {
                        service.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
                    }
                }
                try {
                    Thread.sleep(3000);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public int serviceMain(String[] args){

        while(true) {
            final service service = new service();
            try {
                service.createDefaultProvider();
                service.connect();
                Runtime.getRuntime().addShutdownHook(
                        new Thread() {
                            public void run() {
                                try {
                                    service.close();
                                } catch (IOException e) {
                                }
                            }
                        }
                );
                while (true) {
                    Socket clientSocket = service.Socket.accept();
                    client client = new client(clientSocket);
                    client.start();
                }
            } catch (Exception e) {
                service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
                e.printStackTrace();
            } finally {
                if (service.Socket != null && !service.Socket.isClosed()) {
                    try {
                        service.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
                    }
                }
                try {
                    Thread.sleep(3000);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public static boolean killProcess(String serviceName) throws Exception {
        Process p = Runtime.getRuntime().exec("netstat -ano");
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains(serviceName)){
                String[] LineSplit = line.split("       ");
                Runtime.getRuntime().exec("taskkill /f /pid "+LineSplit[LineSplit.length-1]);
                return true;
            }
        }
        return false;
    }

    public static String getConfigFileName(){
        return ConfigFileName;
    }

    public static String getAccessLogFileName(){
        return AccessLogFileName;
    }

    public static String getErrorLogFileName(){
        return ErrorLogFileName;
    }

    public static boolean createConfigFile() throws IOException {
        if(!fileExists(ConfigFileName)){
            createDirectory(ConfigFileName);
            File f = getFile(ConfigFileName);
            GsonBuilder gb = new GsonBuilder();
            Gson parser = gb.disableHtmlEscaping().setPrettyPrinting().create();
            config config = new config();
            config.port = 4001;
            config.reportaddress = "spchatfield@yahoo.com";
            config.email = "silas@doxpress.com";
            config.host = "mail.doxpress.com";
            config.username = "silas@doxpress.com";
            config.password = "*****";
            config.repositories.add(config.newrepo("gitrunner", "master", "bitbucket-push", "cd c:/wamp/www/gitrunner & git pull https://silashatfield:*******@bitbucket.org/silashatfield/gitrunner.git", "1234"));
            PrintWriter o = new PrintWriter(new BufferedWriter(new FileWriter(f, false)));
            o.print(parser.toJson(config));
            o.close();
        }else{
            return false;
        }
        return true;
    }

    public static boolean createDefaultProvider() throws IOException {
        if(!fileExists(ProviderDefaultFileName)){
            createDirectory(ProviderDefaultFileName);
            File f = getFile(ProviderDefaultFileName);
            GsonBuilder gb = new GsonBuilder();
            Gson parser = gb.disableHtmlEscaping().setPrettyPrinting().create();
            provider provider = new provider();
            provider.name = "bitbucket-push";
            provider.headers.add(provider.newheader("User-Agent", "Bitbucket-Webhooks/2.0"));
            provider.routes.add(provider.newroute("repository", "repository=>name"));
            provider.routes.add(provider.newroute("branch", "push=>changes=>0=>new=>name"));
            provider.routes.add(provider.newroute("username", "push=>actor=>display_name"));
            PrintWriter o = new PrintWriter(new BufferedWriter(new FileWriter(f, false)));
            o.print(parser.toJson(provider));
            o.close();
        }else{
            return false;
        }
        return true;
    }

    public static boolean fileExists(String FileName) throws IOException {
        File f = getFile(FileName);
        if(f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }

    public static File getFile(String FileName) throws IOException {
        File directory = new File(".");
        String path = directory.getCanonicalPath() + File.separator + FileName;
        File f = new File(path);
        return f;
    }

    public static boolean createErrorLogFile() throws IOException {
        if(!fileExists(ErrorLogFileName)){
            createDirectory(ErrorLogFileName);
            File f = getFile(ErrorLogFileName);
            f.createNewFile();
        }else{
            return false;
        }
        return true;
    }

    public static boolean createAccessLogFile() throws IOException {
        if(!fileExists(AccessLogFileName)){
            createDirectory(AccessLogFileName);
            File f = getFile(AccessLogFileName);
            f.createNewFile();
        }else{
            return false;
        }
        return true;
    }

    public static void createDirectory(String path){
        String[] directories = path.toString().split("\\\\");
        for (int i = 0; i < directories.length-1; i++){
            File f = new File(directories[i]);
            f.mkdir();
        }
    }

    public static boolean recordError(String message){
        service service = new service();
        try{

            try{
                String to = service.getConfigSettingString("reportaddress");
                String from = service.getConfigSettingString("email");
                String host = service.getConfigSettingString("host");
                Properties properties = System.getProperties();
                properties.setProperty("mail.smtp.host", host);
                properties.put("mail.smtp.auth", "true");
                Authenticator auth = new javax.mail.Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        String username = null;
                        String password = null;
                        try {
                            service service = new service();
                            username = service.getConfigSettingString("username");
                            password = service.getConfigSettingString("password");
                        }catch (Exception e){

                        }
                        return new PasswordAuthentication(username, password);
                    }
                };
                Session session = Session.getDefaultInstance(properties, auth);
                MimeMessage msg = new MimeMessage(session);
                msg.setFrom(new InternetAddress(from));
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                msg.setSubject("GIT Error Notification");
                msg.setText(message);
                Transport.send(msg);
            }catch (MessagingException mex) {
                mex.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

            service.createErrorLogFile();
            File f = getFile(ErrorLogFileName);
            PrintWriter o = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            o.println(timeStamp+" : "+message+System.getProperty("line.separator"));
            o.close();
        } catch (IOException e1) {
            return false;
        }
        return true;
    }

    public static boolean recordAccess(String message){
        try{
            service.createAccessLogFile();
            File f = getFile(AccessLogFileName);
            PrintWriter o = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            o.println(timeStamp+" : "+message+System.getProperty("line.separator"));
            o.close();
        } catch (IOException e1) {
            return false;
        }
        return true;
    }

    public boolean isNumeric(String s) {
        return s.matches("[-+]?\\d*\\.?\\d+");
    }

    public static String parsePath(String json, String path) throws ParseException {
        String result = "";
        String[] keys = path.split("=>");
        if(keys.length > 0) {
            String key = keys[0];
            String newpath = "";
            for(int i = 1; i < keys.length; i++){
                newpath += keys[i] + (i < (keys.length-1) ? "=>" : "");
            }
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(json);
            try {
                JSONObject jo = (JSONObject) obj;
                if(newpath.length() > 0) {
                    result = parsePath(jo.get(key).toString(), newpath);
                }else{
                    result = jo.get(key).toString();
                }
            }catch (ClassCastException e){
                JSONArray jo = (JSONArray) obj;
                if(newpath.length() > 0) {
                    result = parsePath(jo.get(Integer.parseInt(key)).toString(), newpath);
                }else{
                    result = jo.get(Integer.parseInt(key)).toString();
                }
            }
        }
        return result;
    }

    public static int getConfigSettingInt(String key) throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        File f = service.getFile(service.getConfigFileName());
        Gson parser = new Gson();
        BufferedReader br = new BufferedReader(new FileReader(f));
        config config = parser.fromJson(br, config.class);
        br.close();
        Field property = config.getClass().getDeclaredField(key);
        Object value = property.get(config);
        return Integer.parseInt(value.toString());
    }

    public static Object getConfigSettingObj(String key) throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        File f = service.getFile(service.getConfigFileName());
        Gson parser = new Gson();
        BufferedReader br = new BufferedReader(new FileReader(f));
        config config = parser.fromJson(br, config.class);
        br.close();
        Field property = config.getClass().getDeclaredField(key);
        Object value = property.get(config);
        return value;
    }

    public static String getConfigSettingString(String key) throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        File f = service.getFile(service.getConfigFileName());
        Gson parser = new Gson();
        BufferedReader br = new BufferedReader(new FileReader(f));
        config config = parser.fromJson(br, config.class);
        br.close();
        Field property = config.getClass().getDeclaredField(key);
        Object value = property.get(config);
        return value.toString();
    }

    public static boolean connect() throws Exception {
        service.createConfigFile();
        int port = getConfigSettingInt("port");
        service.killProcess(":"+Integer.toString(port));
        Socket = new ServerSocket(port);
        Socket.setReuseAddress(true);
        return Socket.isBound();
    }

    public static boolean close() throws IOException {
        if(Socket.isBound()){
            Socket.close();
        }
        return Socket.isClosed();
    }
}