package com.jrapple.service;

import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
public class client extends Thread{
    Socket Socket = null;
    private volatile boolean isRunning = true;
    private static List<String> Providers = new ArrayList<String>();

    public client(Socket s){
        Socket = s;
    }

    public void run(){
        try {
            if (Socket != null) {
                DefaultHttpServerConnection conn = new DefaultHttpServerConnection();
                conn.bind(Socket, new BasicHttpParams());
                org.apache.http.HttpRequest request = conn.receiveRequestHeader();
                conn.receiveRequestEntity((HttpEntityEnclosingRequest) request);
                HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
                processData(EntityUtils.toString(entity), request.toString());
                PrintWriter out = new PrintWriter(new OutputStreamWriter(Socket.getOutputStream()));
                out.println("HTTP/1.x 200 OK\n");
                out.flush();
                out.close();
                Socket.close();
            }
        } catch (IOException e) {
            service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
            e.printStackTrace();
        } catch (HttpException e) {
            service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
            e.printStackTrace();
        } catch (ClassCastException e){
            service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
            e.printStackTrace();
        } catch (ParseException e) {
            service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
            e.printStackTrace();
        }
    }

    private static void loadProviders() throws IOException {
        File folder = new File("providers");
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String provider = "";
                try {
                    StringBuilder sb = new StringBuilder();
                    String line = br.readLine();
                    while (line != null) {
                        sb.append(line);
                        sb.append(System.lineSeparator());
                        line = br.readLine();
                    }
                    provider = sb.toString();
                } finally {
                    br.close();
                }
                Providers.add(provider);
            }
        }
    }

    private boolean processHeaders(String data, repository repo, provider provider){
        boolean isSecure = true;
        String[] Params = data.split(" ");
        String[] urlParams = Params[1].split("&");
        for(int i=0; i<urlParams.length; i++){
            if(urlParams[i].contains("token")){
                String key = urlParams[i].split("=")[0];
                String value = urlParams[i].split("=")[1];
                if(repo.token.compareTo(value) != 0){
                    service service = new service();
                    service.recordError("Failed security authentication!");
                    return false;
                }
            }
        }
        //TODO: loop over headers in the provider
        return isSecure;
    }

    private void processData(String data,String headers) throws NoSuchFieldException, IllegalAccessException, ParseException, IOException {
        loadProviders();
        Gson parser = new Gson();
        service service = new service();
        ArrayList<repository> repositories = (ArrayList<repository>) service.getConfigSettingObj("repositories");
        int j = 0;
        while (j < Providers.size()) {
            try{
                provider provider = parser.fromJson(Providers.get(j), provider.class);
                for (int x = 0; x < provider.routes.size(); x++) {
                    route thisRoute = provider.routes.get(x);
                    if (thisRoute.name.compareTo("repository") == 0) {
                        String name = service.parsePath(URLDecoder.decode(data, "UTF-8"), thisRoute.path);
                        for (int t = 0; t < repositories.size(); t++) {
                            repository thisRepo = repositories.get(t);
                            if (thisRepo.name.compareTo(name) == 0) {
                                for (int xx = 0; xx < provider.routes.size(); xx++) {
                                    route thisRoute2 = provider.routes.get(xx);
                                    if (thisRoute2.name.compareTo("branch") == 0) {
                                        String branch = service.parsePath(URLDecoder.decode(data, "UTF-8"), thisRoute2.path);
                                        if(thisRepo.branch.compareTo(branch) == 0) {
                                            if (thisRepo.token.length() == 0 || processHeaders(headers, thisRepo, provider)) {
                                                //TODO: more detailed access log using the JSON routes to get name and etc
                                                String accessMessage = name + " on branch " + branch + " was accessed. ";
                                                ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", thisRepo.cmd);
                                                builder.redirectErrorStream(true);
                                                final Process p = builder.start();
                                                cmdbuffer successBuffer = new cmdbuffer(p,"success");
                                                new Thread(successBuffer).start();
                                                p.waitFor();
                                                String bufferMessage = successBuffer.getValue();
                                                service.recordAccess(accessMessage + bufferMessage);
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }catch(Exception e) {
                service.recordError(e.toString() + " " + e.getStackTrace()[2].getLineNumber());
                e.printStackTrace();
            }
            j++;
        }
    }

    public void kill() {
        isRunning = false;
    }

    public boolean getRunning(){
        return isRunning;
    }

}