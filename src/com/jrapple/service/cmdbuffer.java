package com.jrapple.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class cmdbuffer implements Runnable {
    private String output = "";
    private Process p;
    private String t;

    public cmdbuffer(Process process, String type){
        p = process;
        t = type;
    }

    @Override
    public void run() {
        BufferedReader input = null;
        if(t == "error") {
            input = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        }else{
            input = new BufferedReader(new InputStreamReader(p.getInputStream()));
        }
        String s = null;
        try {
            while ((s = input.readLine()) != null) {
                output += " "+s;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getValue() {
        return output;
    }
}