package com.jrapple.service;

import java.util.ArrayList;

class repository{
    String name;
    String provider;
    String cmd;
    String branch;
    String token;

    repository(String name, String branch, String provider, String cmd, String token){
        this.name = name;
        this.provider = provider;
        this.branch = branch;
        this.cmd = cmd;
        this.token = token;
    }
}

public class config {

    int port;
    String reportaddress = "";
    String email = "";
    String host = "";
    String username = "";
    String password = "";
    ArrayList<repository> repositories = new ArrayList<repository>();

    public repository newrepo(String name, String branch, String provider, String cmd, String token){
        return new repository(name, branch, provider, cmd, token);
    }

}
