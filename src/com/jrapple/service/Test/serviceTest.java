package com.jrapple.service.Test;

import com.jrapple.service.service;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static org.junit.Assert.*;

public class serviceTest {
    com.jrapple.service.service service = new service();

    @BeforeClass
    public static void setup(){

    }
    @AfterClass
    public static void teardown() throws IOException {
        service service = new service();
        File f = service.getFile(service.getConfigFileName());
        if(f.exists()){
            f.delete();
        }
        f = service.getFile(service.getErrorLogFileName());
        if(f.exists()){
            f.delete();
        }
        f = service.getFile(service.getAccessLogFileName());
        if(f.exists()){
            f.delete();
        }
    }
    @Test
    public void test_config_file_name(){
        assertTrue(service.getConfigFileName().length() > 0);
    }
    @Test
    public void test_accesslog_file_name(){
        assertTrue(service.getAccessLogFileName().length() > 0);
    }
    @Test
    public void test_errorlog_file_name(){
        assertTrue(service.getErrorLogFileName().length() > 0);
    }
    @Test
    public void test_file_does_not_exist() throws IOException {
        assertFalse(service.fileExists("somerandomfile.txt"));
    }
    @Test
    public void test_get_file_is_file() throws IOException {
        File directory = new File(".");
        assertEquals(service.getFile("somerandomfile.txt"),new File(directory.getCanonicalPath() + File.separator + "somerandomfile.txt"));
    }
    @Test
    public void test_create_config_file() throws IOException {
        File f = service.getFile(service.getConfigFileName());
        if(f.exists()){
            f.delete();
        }
        assertTrue(service.createConfigFile());
    }
    @Test
    public void test_is_numeric() {
        assertTrue(service.isNumeric("4001"));
        assertFalse(service.isNumeric("NOT4001"));
    }
    @Test
    public void test_config_file_get_int() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        File f = service.getFile(service.getConfigFileName());
        if(!f.exists()){
            service.createConfigFile();
        }
        assertTrue(service.getConfigSettingInt("port") > 0);
    }
    @Test
    public void test_config_file_get_string() throws IOException, ParseException, NoSuchFieldException, IllegalAccessException {
        File f = service.getFile(service.getConfigFileName());
        if(!f.exists()){
            service.createConfigFile();
        }
        assertTrue(service.getConfigSettingString("port").length() > 0);
    }
    @Test
    public void test_create_errorlog_file() throws IOException {
        File f = service.getFile(service.getErrorLogFileName());
        if(f.exists()){
            f.delete();
        }
        assertTrue(service.createErrorLogFile());
    }
    @Test
    public void test_write_error_to_file() throws IOException {
        File f = service.getFile(service.getErrorLogFileName());
        if(!f.exists()){
            service.createErrorLogFile();
        }
        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        service.recordError("This is a test error!");
        assertFalse(br.readLine() == null);
        fr.close();
    }
    @Test
    public void test_create_accesslog_file() throws IOException {
        File f = service.getFile(service.getAccessLogFileName());
        if(f.exists()){
            f.delete();
        }
        assertTrue(service.createAccessLogFile());
    }
    @Test
    public void test_socket_connection() throws Exception {
        File f = service.getFile(service.getConfigFileName());
        if(!f.exists()){
            service.createConfigFile();
        }
        assertTrue(service.connect());
        assertTrue(service.close());
    }
}