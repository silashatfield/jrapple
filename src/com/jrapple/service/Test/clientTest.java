package com.jrapple.service.Test;

import com.jrapple.service.*;
import com.jrapple.service.client;
import com.jrapple.service.service;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

import static org.junit.Assert.*;

public class clientTest {
    com.jrapple.service.service service = new service();
    com.jrapple.service.client client;

    @BeforeClass
    public static void setup(){

    }
    @AfterClass
    public static void teardown() throws IOException {
        service service = new service();
        File f = service.getFile(service.getConfigFileName());
        if(f.exists()){
            f.delete();
        }
        f = service.getFile(service.getErrorLogFileName());
        if(f.exists()){
            f.delete();
        }
        f = service.getFile(service.getAccessLogFileName());
        if(f.exists()){
            f.delete();
        }
    }
    @Test
    public void create_new_client_run_and_stop() throws IOException, ParseException {
        Socket clientSocket = new Socket();
        client = new client(clientSocket);
        client.start();
        assertTrue(client.getRunning());
        client.kill();
        assertFalse(client.getRunning());
    }

}